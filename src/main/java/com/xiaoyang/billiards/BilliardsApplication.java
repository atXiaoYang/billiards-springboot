package com.xiaoyang.billiards;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
@MapperScan({"com.**.mapper"})
public class BilliardsApplication {

    public static void main(String[] args) {
        try {
            log.info("火箭台球厅后端管理系统 开始启动 ................");
            SpringApplication.run(BilliardsApplication.class, args);
            log.info("火箭台球厅后端管理系统 启动成功 ................");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("火箭台球厅后端管理系统 启动失败，原因:", e);
        }
    }

}
