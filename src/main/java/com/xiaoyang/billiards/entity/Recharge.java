package com.xiaoyang.billiards.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@TableName("recharge")
@Getter
@Setter
public class Recharge {
    private Integer rechargeId;
    private Integer memberId;
    private BigDecimal rechargeMoney;
    private Timestamp rechargeTime;
}
