package com.xiaoyang.billiards.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@TableName("consume")
@Getter
@Setter
public class Consume {
    private Integer consumeId;
    private Integer memberId;
    private BigDecimal consumeMoney;
    private Timestamp startTime;
    private Timestamp stopTime;
    private Integer keepTime;
    private Integer tableId;
    private Integer zoneId;
}
