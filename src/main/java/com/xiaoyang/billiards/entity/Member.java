package com.xiaoyang.billiards.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@TableName("member")
@Getter
@Setter
public class Member {
    private Integer memberId;
    private String memberName;
    private String phone;
    private BigDecimal balance;
    private Timestamp createTime;
}
