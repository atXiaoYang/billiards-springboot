package com.xiaoyang.billiards.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/2/4
 * @DESCRIPTION TODO
 */
@TableName("user")
@Getter
@Setter
public class User {
    private Integer userId;
    private String userName;
    private String userPassword;
    private String userPhone;
    private int userState;
    private String userCall;

}
