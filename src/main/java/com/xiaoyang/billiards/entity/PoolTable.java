package com.xiaoyang.billiards.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@TableName("pool_table")
@Getter
@Setter
public class PoolTable {
    private Integer tableId;
    private Integer zoneId;
    private Integer state;
    private Timestamp startTime;
}
