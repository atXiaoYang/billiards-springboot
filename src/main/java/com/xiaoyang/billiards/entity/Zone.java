package com.xiaoyang.billiards.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@TableName("zone")
@Getter
@Setter
public class Zone {
    private Integer zoneId;
    private String zoneName;
    private BigDecimal zoneMoney;
    private BigDecimal memberMoney;
}
