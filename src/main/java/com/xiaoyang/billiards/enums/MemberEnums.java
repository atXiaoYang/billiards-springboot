package com.xiaoyang.billiards.enums;

import lombok.Getter;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/6
 * @DESCRIPTION TODO
 */
@Getter
public enum MemberEnums {
    TOURIST(0, "游客"),
    ;
    private final int code;
    private final String desc;

    MemberEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}