package com.xiaoyang.billiards.enums;

import lombok.Getter;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/6
 * @DESCRIPTION TODO
 */
@Getter
public enum UserEnums {
    ADMIN(1, "管理员"),
    ;
    private final int code;
    private final String desc;

    UserEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
