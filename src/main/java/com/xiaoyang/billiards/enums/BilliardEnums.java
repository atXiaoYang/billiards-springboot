package com.xiaoyang.billiards.enums;

import lombok.Getter;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/2/4
 * @DESCRIPTION TODO
 */
@Getter
public enum BilliardEnums {
    // 登陆
    LOGIN(1, "登陆成功"),
    ;

    private final int code;
    private final String desc;

    BilliardEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
