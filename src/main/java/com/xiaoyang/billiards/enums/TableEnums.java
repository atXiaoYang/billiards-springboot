package com.xiaoyang.billiards.enums;

import lombok.Getter;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Getter
public enum TableEnums {
    ADD_STATE(0, "启用"),
    ;
    private final int code;
    private final String desc;

    TableEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
