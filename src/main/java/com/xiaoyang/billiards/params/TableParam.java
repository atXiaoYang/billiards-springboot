package com.xiaoyang.billiards.params;

import com.xiaoyang.billiards.params.check.ParamCheck;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@Getter
@Setter
public class TableParam implements ParamCheck {

    private Integer zoneId;
    private Integer tableId;
    private Integer state;

    /*
            {
                "member_id":"2024020401 | 0",
                "start_time":"2024-03-04 10:24:39",
                "table_id":24,
                "zone_id":1,
                "state":false,
                "keep_time":1,
                "consume_money":8
             }
         */
    private Integer memberId;
    private Timestamp startTime;
    private Integer keepTime;
    private BigDecimal consumeMoney;

    @Override
    public void check() {

    }
}
