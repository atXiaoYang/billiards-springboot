package com.xiaoyang.billiards.params;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Getter
@Setter
public class RechargeParam {
    private Integer memberId;
    private BigDecimal balance;
    private BigDecimal recharge;
    private Integer rechargeId;
}
