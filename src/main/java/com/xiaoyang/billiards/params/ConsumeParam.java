package com.xiaoyang.billiards.params;

import com.xiaoyang.billiards.params.check.ParamCheck;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Getter
@Setter
public class ConsumeParam implements ParamCheck {

    private Integer zoneId;
    private Integer tableId;
    private Integer state;
    private Integer memberId;
    private Timestamp startTime;
    private Integer keepTime;
    private BigDecimal consumeMoney;

    private Integer consumeId;

    @Override
    public void check() {

    }
}
