package com.xiaoyang.billiards.params;

import com.xiaoyang.billiards.architecture.utils.CheckUtil;
import com.xiaoyang.billiards.params.check.ParamCheck;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@Getter
@Setter
public class ZoneParam implements ParamCheck {
    private Integer zoneId;
    private String zoneName;
    private BigDecimal zoneMoney;
    private BigDecimal memberMoney;
    @Override
    public void check() {
        CheckUtil.assertGteZero(zoneMoney,"请输入正整数");
        CheckUtil.assertGteZero(memberMoney,"请输入正整数");
    }
}
