package com.xiaoyang.billiards.params.check;

import java.io.Serializable;

/**
 * @author 杨泽纬
 * @description 参数校验
 * @serviceName TestParam
 * @date 2023/11/16
 */
public interface ParamCheck extends Serializable {
    void check();
}
