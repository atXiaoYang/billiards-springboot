package com.xiaoyang.billiards.params;

import com.xiaoyang.billiards.architecture.utils.CheckUtil;
import com.xiaoyang.billiards.params.check.ParamCheck;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 杨泽纬
 * @description 测试参数
 * @serviceName TestParam
 * @date 2023/11/16
 */
@Getter
@Setter
public class UserParam implements ParamCheck {

    private Integer userId;
    private String name;
    private String password;
    private String userPhone;
    private Integer userState;
    private String userCall;
    private String token;

    //用户页面
    private String userName;
    private String userPassword;

    @Override
    public void check() {
        CheckUtil.assertNotEmpty(name, "账号不能为空");
        CheckUtil.assertNotEmpty(password, "密码不能为空");
    }
}
