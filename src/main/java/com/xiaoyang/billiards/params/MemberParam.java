package com.xiaoyang.billiards.params;

import com.xiaoyang.billiards.params.check.ParamCheck;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Getter
@Setter
public class MemberParam implements ParamCheck {

    private Integer zoneId;
    private Integer tableId;
    private Integer state;

    private String memberName;
    private String phone;

    private Timestamp startTime;
    private Integer keepTime;
    private BigDecimal consumeMoney;

    private Integer memberId;
    private BigDecimal balance;
    private BigDecimal recharge;

    @Override
    public void check() {

    }
}
