package com.xiaoyang.billiards.transfer;

import com.xiaoyang.billiards.entity.Member;
import com.xiaoyang.billiards.response.MemberResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Mapper
public interface MemberTransfer {
    MemberTransfer INSTANCE = Mappers.getMapper(MemberTransfer.class);

    @Mappings({})
    List<MemberResponse> transfer(List<Member> members);

    @Mappings({})
    MemberResponse transfer(Member member);
}
