package com.xiaoyang.billiards.transfer;

import com.xiaoyang.billiards.entity.Consume;
import com.xiaoyang.billiards.response.ConsumeResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/5
 * @DESCRIPTION TODO
 */
@Mapper
public interface ConsumeTransfer {
    ConsumeTransfer INSTANCE = Mappers.getMapper(ConsumeTransfer.class);

    List<ConsumeResponse> transfer(List<Consume> consumes);
}
