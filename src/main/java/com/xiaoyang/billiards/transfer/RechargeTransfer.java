package com.xiaoyang.billiards.transfer;

import com.xiaoyang.billiards.entity.Recharge;
import com.xiaoyang.billiards.response.RechargeResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/5
 * @DESCRIPTION TODO
 */
@Mapper
public interface RechargeTransfer {
    RechargeTransfer INSTANCE = Mappers.getMapper(RechargeTransfer.class);

    List<RechargeResponse> transfer(List<Recharge> recharges);
}
