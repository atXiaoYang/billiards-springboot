package com.xiaoyang.billiards.transfer;

import com.xiaoyang.billiards.response.TableResponse;
import com.xiaoyang.billiards.response.ZoneResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@Mapper
public interface TableTransfer {
    TableTransfer INSTANCE = Mappers.getMapper(TableTransfer.class);

    @Mappings({})
    List<TableResponse> transfer(List<ZoneResponse> list);
}
