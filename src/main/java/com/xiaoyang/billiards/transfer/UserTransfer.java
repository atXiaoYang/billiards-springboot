package com.xiaoyang.billiards.transfer;

import com.xiaoyang.billiards.entity.User;
import com.xiaoyang.billiards.params.UserParam;
import com.xiaoyang.billiards.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/2/4
 * @DESCRIPTION TODO
 */
@Mapper
public interface UserTransfer {

    UserTransfer INSTANCE = Mappers.getMapper(UserTransfer.class);

    @Mappings({})
    UserResponse transfer(UserParam userParam);

    @Mappings({})
    UserResponse transfer(User user);

    @Mappings({})
    List<UserResponse> transfer(List<User> users);
}
