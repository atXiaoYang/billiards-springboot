package com.xiaoyang.billiards.architecture.common.Errors;

/**
 * @author 杨泽纬
 * @description 业务异常
 * @serviceName BizError
 * @date 2023/11/16
 */
public enum ServiceError implements Errors {
    SYSTEM_ERROR(-9999, "未知错误,请稍后重试:{0}"),
    PARAM_ERROR(-4, "参数异常:{0}"),
    ANNOTATION_ERROR(100020, "注解异常:{0}");
    private final int code;
    private final String msg;

    ServiceError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public String getErrorCode() {
        return name();
    }
}

