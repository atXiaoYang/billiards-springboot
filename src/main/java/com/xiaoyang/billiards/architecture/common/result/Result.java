package com.xiaoyang.billiards.architecture.common.result;

import com.xiaoyang.billiards.architecture.common.Errors.Errors;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import lombok.Getter;

import java.text.MessageFormat;

/**
 * @author 杨泽纬
 * @description 统一返回结果类
 * @serviceName Result
 * @date 2023/11/16
 */
public class Result<T> implements Errors {
    /**
     * 异常数字编码,全局唯一，成功为0
     * */
    private final int code;
    /**
     * 异常英文编码，全局唯一，成功为空
     * */
    private final String errorCode;
    /**
     * 服务端已格式化好的异常文案
     * */
    private final String msg;
    /**
     * 异常文案格式化参数
     * */
    @Getter
    private final String[] msgParam;
    /**
     * 返回的业务数据对象
     * */
    @Getter
    private final T data;

//    /**
//     * 返回业务对象的动态数据
//     */
//    @Getter
//    private final Map<String, Object> map;

    public Result(T data) {
        this.code = 201;
        this.msg = null;
        this.msgParam = null;
        this.errorCode = null;
        this.data = data;
    }

    public Result(T data, String msg, String[] msgParam) {
        this.code = 201;
        this.msg = msg;
        this.msgParam = msgParam;
        this.errorCode = null;
        this.data = data;
    }

    public Result(ServiceException e) {
        this.code = e.getCode();
        this.msg = e.getMsg();
        this.msgParam = e.getMsgParam();
        this.errorCode = e.getErrorCode();
        this.data = null;
    }

    public Result(T data, ServiceException e) {
        this.code = e.getCode();
        this.msg = e.getMsg();
        this.msgParam = e.getMsgParam();
        this.errorCode = e.getErrorCode();
        this.data = data;
    }

    public Result(Errors errors) {
        this.code = errors.getCode();
        this.msg = errors.getMsg();
        this.msgParam = null;
        this.errorCode = errors.getErrorCode();
        this.data = null;
    }

    public Result(T data, Errors errors) {
        this.code = errors.getCode();
        this.msg = errors.getMsg();
        this.msgParam = null;
        this.errorCode = errors.getErrorCode();
        this.data = data;
    }

    public Result(Errors errors, Object... args) {
        this.code = errors.getCode();
        this.errorCode = errors.getErrorCode();
        this.msg = MessageFormat.format(errors.getMsg(), args);
        String[] param = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            param[i] = args[i].toString();
        }
        this.msgParam = param;
        this.data = null;
    }

    public Result(T data, Errors errors, Object... args) {
        this.code = errors.getCode();
        this.msg = MessageFormat.format(errors.getMsg(), args);
        String[] param = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            param[i] = args[i].toString();
        }
        this.msgParam = param;
        this.errorCode = errors.getErrorCode();
        this.data = data;
    }

    private Result() {
        this.code = 201;
        this.errorCode = null;
        this.msg = null;
        this.msgParam = null;
        this.data = null;
    }

    // 正确信息开始
    public static <T> Result<T> ok(T data) {
        return new Result<>(data);
    }

    public static <T> Result<T> ok() {
        return new Result<>();
    }

    // 正确信息结束

    // 错误信息开始
    public static <T> Result<T> error(Errors errors) {
        return new Result<>(errors);
    }

    public static <T> Result<T> error(T data, Errors errors) {
        return new Result<>(data, errors);
    }

    public static <T> Result<T> error(Errors errors, Object... args) {
        return new Result<>(errors, args);
    }

    public static <T> Result<T> error(ServiceException e) {
        return new Result<>(e);
    }

    public static <T> Result<T> error(T data, ServiceException e) {
        return new Result<>(data, e);
    }

    public static <T> Result<T> error(T data, Errors errors, Object... args) {
        return new Result<>(data, errors, args);
    }

    // 错误信息结束

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public String getErrorCode() {
        return errorCode;
    }

    public boolean isSuccess() {
        return code == 0;
    }

}

