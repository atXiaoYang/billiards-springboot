package com.xiaoyang.billiards.architecture.common.Errors;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
public enum TableError implements Errors{
    ADD_ERROR(199, "添加错误:{0}"),
    DELETE_ERROR(198, "删除错误:{0}"),
    START_ERROR(197, "开始错误:{0}"),
    STOP_ERROR(196, "停止错误:{0}"),
    ;

    private final int code;
    private final String msg;

    TableError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public String getErrorCode() {
        return name();
    }
}
