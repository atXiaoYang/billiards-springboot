package com.xiaoyang.billiards.architecture.common.Errors;

/**
 * @author 杨泽纬
 * @description TODO
 * @serviceName Errors
 * @date 2023/11/16
 */
public interface Errors {

    int getCode();

    String getErrorCode();

    String getMsg();
}

