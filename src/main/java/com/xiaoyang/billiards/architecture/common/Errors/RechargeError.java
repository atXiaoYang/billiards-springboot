package com.xiaoyang.billiards.architecture.common.Errors;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/5
 * @DESCRIPTION TODO
 */
public enum RechargeError implements Errors{

    ADD_ERROR(199, "添加错误:{0}"),
    DELETE_ERROR(198, "删除错误:{0}"),
    RECHARGE_ERROR(180, "充值错误:{0}"),

    ;

    private final int code;
    private final String msg;

    RechargeError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getErrorCode() {
        return name();
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
