package com.xiaoyang.billiards.architecture.common.Errors;

/**
 * @author 杨泽纬
 * @description 测试异常
 * @serviceName IndexError
 * @date 2023/11/16
 */
public enum IndexError implements Errors{

    INDEX_ERROR(10000000, "测试错误 : {0}"),
    ;

    private final int code;
    private final String msg;

    IndexError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getErrorCode() {
        return name();
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
