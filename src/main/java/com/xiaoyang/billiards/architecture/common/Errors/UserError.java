package com.xiaoyang.billiards.architecture.common.Errors;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
public enum UserError implements Errors{
    ADD_ERROR(199, "添加错误:{0}"),
    DELETE_ERROR(198, "删除错误:{0}"),
    UPDATE_ERROR(190,"更新失败:{0}"),
    TOKEN_ERROR(505, "认证失败:{0}"),
    ;

    private final int code;
    private final String msg;

    UserError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public String getErrorCode() {
        return name();
    }
}
