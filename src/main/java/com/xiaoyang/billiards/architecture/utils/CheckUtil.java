package com.xiaoyang.billiards.architecture.utils;

import com.xiaoyang.billiards.architecture.common.Errors.ServiceError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;

import java.math.BigDecimal;

/**
 * @author 杨泽纬
 * @description 检查工具类
 * @serviceName CheckUtil
 * @date 2023/11/16
 */
public class CheckUtil {

    public static void assertGteZero(Integer obj, String error) {
        if (obj == null || obj <= 0) {
            throw new ServiceException(ServiceError.PARAM_ERROR, error);
        }
    }

    public static void assertGteZero(BigDecimal obj, String error) {
        if (obj == null || obj.compareTo(BigDecimal.ZERO) < 0) {
            throw new ServiceException(ServiceError.PARAM_ERROR, error);
        }
    }

    public static Boolean assertNotEmpty(String str, String error) {
        if (str == null || str.isEmpty()) {
            throw new ServiceException(ServiceError.PARAM_ERROR, error);
        }
        return true;
    }

    public static Boolean assertNotEmpty(Integer integer, String error) {
        if (integer == null) {
            throw new ServiceException(ServiceError.PARAM_ERROR, error);
        }
        return true;
    }

}

