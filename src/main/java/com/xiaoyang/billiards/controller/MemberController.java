package com.xiaoyang.billiards.controller;

import com.xiaoyang.billiards.architecture.common.Errors.MemberError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.architecture.common.result.Result;
import com.xiaoyang.billiards.enums.MemberEnums;
import com.xiaoyang.billiards.params.MemberParam;
import com.xiaoyang.billiards.response.MemberResponse;
import com.xiaoyang.billiards.service.MemberService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class MemberController {

    @Resource
    private MemberService memberService;

    //会员用户扣费
    @PostMapping("/memberConsume")
    public Result<Void> memberConsume(@RequestBody MemberParam memberParam){
        // 如果是会员则通过memberId查询出该会员的余额，如果余额不足，抛出异常
        if (memberParam.getMemberId() != MemberEnums.TOURIST.getCode()){
            MemberResponse memberResponse = memberService.query(memberParam);
            if (memberResponse.getBalance().compareTo(memberParam.getConsumeMoney()) < 0){
                throw new ServiceException(MemberError.MEMBER_BALANCE_NOT_ENOUGH,"余额不足");
            }
        }
        // update member set balance = balance - ? where member_id = ?
        memberService.memberConsume(memberParam);
        return Result.ok();
    }

    @PostMapping("/member")
    public Result<List<MemberResponse>> getMember(){
        List<MemberResponse> memberList = memberService.queryForList(null);
        return Result.ok(memberList);
    }
    @PostMapping("/addMember")
    public Result<String> addMember(@RequestBody MemberParam memberParam){
        Integer result = memberService.add(memberParam);
        return Result.ok("添加成功");
    }

    @PostMapping("/editMember")
    public Result<String> editMember(@RequestBody MemberParam memberParam){
        // update member set member_name = ?,phone = ? where member_id = ?
        Integer result = memberService.updateByMemberParam(memberParam);
        return Result.ok("修改成功");
    }

    // 会员充值
    @PostMapping("/memberRecharge")
    public Result<String> memberRecharge(@RequestBody MemberParam memberParam){
        // update member set balance = ? where member_id = ?
        if (memberParam.getRecharge() == null){
            throw new ServiceException(MemberError.RECHARGE_ERROR,"充值金额不能为空");
        }
        memberParam.setBalance(memberParam.getBalance().add(memberParam.getRecharge()));
        memberService.updateByMemberParam(memberParam);
        return Result.ok("充值成功");
    }

    @PostMapping("/deleteMember")
    public Result<String> deleteMember(@RequestBody MemberParam memberParam){
        // delete from member where member_id = ?
        Integer result = memberService.delete(memberParam);
        return Result.ok("删除成功");
    }

    @PostMapping("/batchDeleteMember")
    public Result<String> batchDeleteMember(@RequestBody List<Integer> memberIds){
        // delete from member where member_id in (?)
        if (memberIds.isEmpty()){
            throw new ServiceException(MemberError.DELETE_ERROR,"用户id不能为空");
        }
        Integer result = memberService.batchDelete(memberIds);
        return Result.ok("删除成功");
    }

}
