package com.xiaoyang.billiards.controller;

import com.xiaoyang.billiards.architecture.common.Errors.MemberError;
import com.xiaoyang.billiards.architecture.common.Errors.RechargeError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.architecture.common.result.Result;
import com.xiaoyang.billiards.params.RechargeParam;
import com.xiaoyang.billiards.response.RechargeResponse;
import com.xiaoyang.billiards.service.RechargeService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class RechargeController {

    @Resource
    private RechargeService rechargeService;

    //生成充值记录
    @PostMapping("/setRecharge")
    public Result<String> setRecharge(@RequestBody RechargeParam rechargeParam){
        // insert into recharge set member_id = ?,recharge_money = ?,recharge_time = now()
        if (rechargeParam.getRecharge() == null){
            throw new ServiceException(RechargeError.RECHARGE_ERROR,"充值金额不能为空");
        }
        Integer recharge = rechargeService.add(rechargeParam);
        return Result.ok("充值成功");
    }

    @PostMapping("/recharge")
    public Result<List<RechargeResponse>> recharge(){
        List<RechargeResponse> rechargeList = rechargeService.queryForList(null);
        return Result.ok(rechargeList);
    }

    @PostMapping("/deleteRecharge")
    public Result<String> deleteRecharge(@RequestBody RechargeParam rechargeParam){
        Integer recharge = rechargeService.delete(rechargeParam);
        return Result.ok("删除成功");
    }

    @PostMapping("/batchDeleteRecharge")
    public Result<String> batchDeleteRecharge(@RequestBody List<Integer> rechargeIds){
        if (rechargeIds.isEmpty()){
            throw new ServiceException(RechargeError.DELETE_ERROR,"用户id不能为空");
        }
        Integer recharge = rechargeService.batchDelete(rechargeIds);
        return Result.ok("删除成功");
    }
}
