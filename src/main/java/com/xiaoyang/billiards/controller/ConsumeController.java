package com.xiaoyang.billiards.controller;

import com.xiaoyang.billiards.architecture.common.Errors.ConsumeError;
import com.xiaoyang.billiards.architecture.common.Errors.MemberError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.architecture.common.result.Result;
import com.xiaoyang.billiards.params.ConsumeParam;
import com.xiaoyang.billiards.response.ConsumeResponse;
import com.xiaoyang.billiards.service.ConsumeService;
import com.xiaoyang.billiards.service.TableService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class ConsumeController {

    @Resource
    private ConsumeService consumeService;

    @PostMapping("/setConsume")
    public Result<String> setConsume(@RequestBody ConsumeParam consumeParam) {
        consumeService.add(consumeParam);
        return Result.ok();
    }

    @PostMapping("/consume")
    public Result<List<ConsumeResponse>> consume() {
        List<ConsumeResponse> consumeResponseList = consumeService.queryForList(null);
        return Result.ok(consumeResponseList);
    }

    @PostMapping("/deleteConsume")
    public Result<String> deleteConsume(@RequestBody ConsumeParam consumeParam) {
        // consumeId
        Integer integer = consumeService.delete(consumeParam);
        return Result.ok("删除成功");
    }

    @PostMapping("/batchDeleteConsume")
    public Result<String> batchDeleteConsume(@RequestBody List<Integer> consumeIds){
        // delete from member where member_id in (?)
        if (consumeIds.isEmpty()){
            throw new ServiceException(ConsumeError.DELETE_ERROR,"用户id不能为空");
        }
        Integer result = consumeService.batchDelete(consumeIds);
        return Result.ok("删除成功");
    }

}
