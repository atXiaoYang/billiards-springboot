package com.xiaoyang.billiards.controller;

import com.xiaoyang.billiards.architecture.common.result.Result;
import com.xiaoyang.billiards.response.analyse.AddmunResponse;
import com.xiaoyang.billiards.response.analyse.DurationResponse;
import com.xiaoyang.billiards.response.analyse.InmoneyResponse;
import com.xiaoyang.billiards.response.analyse.TurnoverResponse;
import com.xiaoyang.billiards.service.ConsumeService;
import com.xiaoyang.billiards.service.MemberService;
import com.xiaoyang.billiards.service.RechargeService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/5
 * @DESCRIPTION TODO
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class AnalyseController {

    @Resource
    private RechargeService rechargeService;

    @Resource
    private MemberService memberService;

    @Resource
    private ConsumeService consumeService;

    //获取近半年每个月会员充值总和
    @PostMapping("/inmoney")
    public Result<List<InmoneyResponse>> inmoney() {
        // SELECT date_format( recharge_time, '%Y-%m' ) month,sum(recharge_money)sum_money FROM recharge WHERE recharge_time BETWEEN date_sub( LAST_DAY( CURDATE() ), INTERVAL 6 MONTH ) AND now() GROUP BY month ORDER BY month ASC
        List<InmoneyResponse> inmoneyResponseList = rechargeService.inmoney();
        return Result.ok(inmoneyResponseList);
    }

    // 获取近半年每个月的营业额
    @PostMapping("/addmun")
    public Result<List<AddmunResponse>> addmun(){
        // SELECT date_format( create_time, '%Y-%m' ) month,COUNT( member_id ) number FROM member WHERE create_time BETWEEN date_sub( LAST_DAY( CURDATE() ), INTERVAL 6 MONTH ) AND now() GROUP BY month ORDER BY month ASC
        return Result.ok(memberService.addmun());
    }

    // 获取每个球桌使用的总时长
    @PostMapping("/duration")
    public Result<List<DurationResponse>> duration() {
        // select table_id, sum(keep_time)sum_time from `consume` group by table_id order by table_id asc
        return Result.ok(consumeService.duration());
    }

    // 获取近半年每个月的营业额
    @PostMapping("/turnover")
    public Result<List<TurnoverResponse>> turnover() {
        // SELECT date_format( start_time, '%Y-%m' ) month,sum( consume_money ) turnover FROM consume WHERE start_time BETWEEN date_sub( LAST_DAY( CURDATE() ), INTERVAL 6 MONTH ) AND now() GROUP BY month ORDER BY month ASC
        return Result.ok(consumeService.turnover());
    }
}
