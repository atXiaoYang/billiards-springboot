package com.xiaoyang.billiards.controller;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/7
 * @DESCRIPTION TODO
 */
@Controller
@RequestMapping("/api")
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class GameController {
    @GetMapping("/game")
    public String game(Integer tableId, HttpServletRequest httpServlet) {
        httpServlet.setAttribute("tableId", "火箭台球厅: " + tableId + "号球桌-正在游戏...");
        // 返回新页面的视图名称
        return "index";
    }
}
