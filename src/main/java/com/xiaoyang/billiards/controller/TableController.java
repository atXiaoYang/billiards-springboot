package com.xiaoyang.billiards.controller;

import com.xiaoyang.billiards.architecture.common.Errors.TableError;
import com.xiaoyang.billiards.architecture.common.result.Result;
import com.xiaoyang.billiards.params.TableParam;
import com.xiaoyang.billiards.response.TableResponse;
import com.xiaoyang.billiards.response.ZoneResponse;
import com.xiaoyang.billiards.service.TableService;
import com.xiaoyang.billiards.service.ZoneService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class TableController {

    @Resource
    private TableService tableService;

    @PostMapping("/table")
    public Result<List<TableResponse>> table() {
        List<TableResponse> list = tableService.query(null);
        return Result.ok(list);
    }

    @PostMapping("/addTable")
    public Result<String> addTable(@RequestBody TableParam tableParam) {
        tableService.add(tableParam);
        return Result.ok("添加成功");
    }

    @PostMapping("/deleteTable")
    public Result<String> deleteTable(@RequestBody TableParam tableParam) {
        tableService.delete(tableParam);
        return Result.ok("删除成功");
    }

    @PostMapping("/start")
    public Result<String> start(@RequestBody TableParam tableParam) {
        // state tableId
        tableService.start(tableParam);
        return Result.ok("开始计时");
    }

    @PostMapping("/stop")
    public Result<String> stop(@RequestBody TableParam tableParam) {
        // {"code":201,"msg":"停止计时"}
        tableService.stop(tableParam);
        return Result.ok("停止计时");
    }


}
