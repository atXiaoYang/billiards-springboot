package com.xiaoyang.billiards.controller;

import com.xiaoyang.billiards.architecture.common.result.Result;
import com.xiaoyang.billiards.params.ZoneParam;
import com.xiaoyang.billiards.response.ZoneResponse;
import com.xiaoyang.billiards.service.ZoneService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class ZoneController {

    @Resource
    private ZoneService zoneService;

    @PostMapping("/zone")
    public Result<List<ZoneResponse>> zone() {
        List<ZoneResponse> list = zoneService.query(null);
        return Result.ok(list);
    }

    @PostMapping("/editZone")
    public Result<String> editZone(@RequestBody ZoneParam zoneParam) {
        zoneService.updateByZoneParam(zoneParam);
        return Result.ok("修改成功");
    }

    @PostMapping("/addZone")
    public Result<String> addZone(@RequestBody ZoneParam zoneParam) {
        zoneService.add(zoneParam);
        return Result.ok("添加成功");
    }

    @PostMapping("/deleteZone")
    public Result<String> deleteZone(@RequestBody ZoneParam zoneParam) {
        zoneService.delete(zoneParam);
        return Result.ok("删除成功");
    }
}
