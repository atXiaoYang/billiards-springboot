package com.xiaoyang.billiards.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoyang.billiards.entity.User;
import com.xiaoyang.billiards.params.UserParam;
import com.xiaoyang.billiards.response.UserResponse;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/2/4
 * @DESCRIPTION TODO
 */
public interface UserService extends IService<User> {
    UserResponse query(UserParam userParam);
    List<UserResponse> queryForList(UserParam userParam);

    Integer updateByUserParam(UserParam userParam);

    Integer add(UserParam userParam);

    Integer delete(UserParam userParam);

    Integer batchDelete(List<Integer> userIds);
}
