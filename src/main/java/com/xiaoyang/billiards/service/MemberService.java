package com.xiaoyang.billiards.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoyang.billiards.entity.Consume;
import com.xiaoyang.billiards.entity.Member;
import com.xiaoyang.billiards.params.MemberParam;
import com.xiaoyang.billiards.params.TableParam;
import com.xiaoyang.billiards.response.MemberResponse;
import com.xiaoyang.billiards.response.TableResponse;
import com.xiaoyang.billiards.response.analyse.AddmunResponse;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
public interface MemberService extends IService<Member> {

    List<MemberResponse> queryForList(MemberParam memberParam);

    MemberResponse query(MemberParam memberParam);

    Integer updateByMemberParam(MemberParam memberParam);

    Integer add(MemberParam memberParam);

    Integer memberConsume(MemberParam memberParam);

    Integer delete(MemberParam memberParam);

    Integer batchDelete(List<Integer> memberIds);

    List<AddmunResponse> addmun();
}
