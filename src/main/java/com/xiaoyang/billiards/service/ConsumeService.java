package com.xiaoyang.billiards.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoyang.billiards.entity.Consume;
import com.xiaoyang.billiards.entity.PoolTable;
import com.xiaoyang.billiards.params.ConsumeParam;
import com.xiaoyang.billiards.response.ConsumeResponse;
import com.xiaoyang.billiards.response.analyse.DurationResponse;
import com.xiaoyang.billiards.response.analyse.TurnoverResponse;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
public interface ConsumeService extends IService<Consume> {
    Integer add(ConsumeParam consumeParam);

    List<ConsumeResponse> queryForList(ConsumeParam consumeParam);

    Integer delete(ConsumeParam consumeParam);

    Integer batchDelete(List<Integer> consumeIds);

    List<DurationResponse> duration();

    List<TurnoverResponse> turnover();
}
