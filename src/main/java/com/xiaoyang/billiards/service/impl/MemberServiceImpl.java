package com.xiaoyang.billiards.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoyang.billiards.architecture.common.Errors.MemberError;
import com.xiaoyang.billiards.architecture.common.Errors.ZoneError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.architecture.utils.CheckUtil;
import com.xiaoyang.billiards.entity.Member;
import com.xiaoyang.billiards.entity.PoolTable;
import com.xiaoyang.billiards.entity.Zone;
import com.xiaoyang.billiards.enums.MemberEnums;
import com.xiaoyang.billiards.mapper.MemberMapper;
import com.xiaoyang.billiards.params.MemberParam;
import com.xiaoyang.billiards.response.MemberResponse;
import com.xiaoyang.billiards.response.analyse.AddmunResponse;
import com.xiaoyang.billiards.service.MemberService;
import com.xiaoyang.billiards.transfer.MemberTransfer;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

    @Resource
    private MemberMapper memberMapper;

    @Override
    public List<MemberResponse> queryForList(MemberParam memberParam) {
        // select * from member where member_id != 0
        LambdaQueryWrapper<Member> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(Member::getMemberId, MemberEnums.TOURIST.getCode());
        List<Member> members = memberMapper.selectList(wrapper);
        return MemberTransfer.INSTANCE.transfer(members);
    }

    @Override
    public MemberResponse query(MemberParam memberParam) {
        LambdaQueryWrapper<Member> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(memberParam.getMemberId() != null, Member::getMemberId,memberParam.getMemberId());
        Member member = memberMapper.selectOne(wrapper);
        return MemberTransfer.INSTANCE.transfer(member);
    }

    @Override
    public Integer updateByMemberParam(MemberParam memberParam) {
        LambdaUpdateWrapper<Member> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(memberParam.getBalance() != null && memberParam.getBalance().compareTo(BigDecimal.ZERO) != 0, Member::getBalance,memberParam.getBalance());
        wrapper.set(StringUtils.hasLength(memberParam.getMemberName()), Member::getMemberName,memberParam.getMemberName());
        wrapper.set(StringUtils.hasLength(memberParam.getPhone()), Member::getPhone,memberParam.getPhone());
        wrapper.eq(CheckUtil.assertNotEmpty(memberParam.getMemberId(),"更新的memberId为空"), Member::getMemberId,memberParam.getMemberId());
        return memberMapper.update(wrapper);
    }

    @Override
    public Integer add(MemberParam memberParam) {
        // insert into member set ?,create_time = now()
        Member member = new Member();
        member.setMemberId(memberParam.getMemberId());
        member.setMemberName(memberParam.getMemberName());
        member.setPhone(memberParam.getPhone());
        member.setBalance(memberParam.getBalance());
        member.setCreateTime(new Timestamp(System.currentTimeMillis()));
        try {
            return memberMapper.insert(member);
        } catch (Exception e) {
            throw new ServiceException(MemberError.ADD_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer memberConsume(MemberParam memberParam) {
        LambdaUpdateWrapper<Member> wrapper = new LambdaUpdateWrapper<>();
        wrapper.setSql(memberParam.getConsumeMoney() != null && memberParam.getConsumeMoney().compareTo(BigDecimal.ZERO) != 0, "balance = balance - {0}", memberParam.getConsumeMoney());
        wrapper.eq(CheckUtil.assertNotEmpty(memberParam.getMemberId(),"更新的memberId为空"), Member::getMemberId,memberParam.getMemberId());
        return memberMapper.update(wrapper);
    }

    @Override
    public Integer delete(MemberParam memberParam) {
        LambdaQueryWrapper<Member> wrapper = new LambdaQueryWrapper<Member>();
        wrapper.eq(Member::getMemberId, memberParam.getMemberId());
        try {
            return memberMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(MemberError.DELETE_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer batchDelete(List<Integer> memberIds) {
        LambdaQueryWrapper<Member> wrapper = new LambdaQueryWrapper<Member>();
        wrapper.in(Member::getMemberId, memberIds);
        memberMapper.delete(wrapper);
        return null;
    }

    @Override
    public List<AddmunResponse> addmun() {
        return memberMapper.addmun();
    }
}
