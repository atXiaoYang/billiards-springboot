package com.xiaoyang.billiards.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoyang.billiards.architecture.common.Errors.RechargeError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.entity.Member;
import com.xiaoyang.billiards.entity.Recharge;
import com.xiaoyang.billiards.mapper.MemberMapper;
import com.xiaoyang.billiards.mapper.RechargeMapper;
import com.xiaoyang.billiards.params.RechargeParam;
import com.xiaoyang.billiards.response.RechargeResponse;
import com.xiaoyang.billiards.response.analyse.InmoneyResponse;
import com.xiaoyang.billiards.service.MemberService;
import com.xiaoyang.billiards.service.RechargeService;
import com.xiaoyang.billiards.transfer.RechargeTransfer;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Service
public class RechargeServiceImpl extends ServiceImpl<RechargeMapper, Recharge> implements RechargeService {

    @Resource
    private RechargeMapper rechargeMapper;

    @Override
    public Integer add(RechargeParam rechargeParam) {
        // insert into recharge set member_id = ?,recharge_money = ?,recharge_time = now()
        Recharge recharge = new Recharge();
        recharge.setMemberId(rechargeParam.getMemberId());
        recharge.setRechargeMoney(rechargeParam.getRecharge());
        recharge.setRechargeTime(new Timestamp(System.currentTimeMillis()));
        return rechargeMapper.insert(recharge);

    }

    @Override
    public List<RechargeResponse> queryForList(RechargeParam rechargeParam) {
        LambdaQueryWrapper<Recharge> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Recharge::getRechargeTime);
        List<Recharge> recharges = rechargeMapper.selectList(wrapper);
        return RechargeTransfer.INSTANCE.transfer(recharges);
    }

    @Override
    public Integer delete(RechargeParam rechargeParam) {
        LambdaQueryWrapper<Recharge> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(rechargeParam.getRechargeId() != null, Recharge::getRechargeId, rechargeParam.getRechargeId());
        try {
            return rechargeMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(RechargeError.DELETE_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer batchDelete(List<Integer> rechargeIds) {
        LambdaQueryWrapper<Recharge> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(Recharge::getRechargeId,rechargeIds);
        try {
            return rechargeMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(RechargeError.DELETE_ERROR,e.getMessage());
        }
    }

    @Override
    public List<InmoneyResponse> inmoney() {
        return rechargeMapper.inmoney();
    }
}
