package com.xiaoyang.billiards.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoyang.billiards.architecture.common.Errors.ZoneError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.architecture.utils.CheckUtil;
import com.xiaoyang.billiards.entity.PoolTable;
import com.xiaoyang.billiards.entity.Zone;
import com.xiaoyang.billiards.mapper.ZoneMapper;
import com.xiaoyang.billiards.params.ZoneParam;
import com.xiaoyang.billiards.response.ZoneResponse;
import com.xiaoyang.billiards.service.ZoneService;
import com.xiaoyang.billiards.transfer.ZoneTransfer;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@Service
public class ZoneServiceImpl extends ServiceImpl<ZoneMapper, Zone> implements ZoneService {

    @Resource
    private ZoneMapper zoneMapper;

    @Override
    public List<ZoneResponse> query(ZoneParam zoneParam) {
        if (ObjectUtils.isEmpty(zoneParam)){
            List<Zone> list = zoneMapper.selectList(null);
            return ZoneTransfer.INSTANCE.transfer(list);
        }
        return null;
    }

    @Override
    public Integer updateByZoneParam(ZoneParam zoneParam) {
        // update zone set zone_name = ?,zone_money = ?,member_money = ? where zone_id = ?
        LambdaUpdateWrapper<Zone> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(StringUtils.hasLength(zoneParam.getZoneName()),Zone::getZoneName,zoneParam.getZoneName());
        wrapper.set(zoneParam.getZoneMoney().compareTo(BigDecimal.ZERO) != 0,Zone::getZoneMoney,zoneParam.getZoneMoney());
        wrapper.set(zoneParam.getMemberMoney().compareTo(BigDecimal.ZERO) != 0,Zone::getMemberMoney,zoneParam.getMemberMoney());
        wrapper.eq(CheckUtil.assertNotEmpty(zoneParam.getZoneId(),"更新的zoneId为空"),Zone::getZoneId,zoneParam.getZoneId());
        try {
            return zoneMapper.update(wrapper);
        } catch (Exception e) {
            throw new ServiceException(ZoneError.UPDATE_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer add(ZoneParam zoneParam) {
        Zone zone = new Zone();
        zone.setZoneMoney(zoneParam.getZoneMoney());
        zone.setZoneName(zoneParam.getZoneName());
        zone.setMemberMoney(zoneParam.getMemberMoney());
        try {
            return zoneMapper.insert(zone);
        } catch (Exception e) {
            throw new ServiceException(ZoneError.ADD_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer delete(ZoneParam zoneParam) {
        LambdaQueryWrapper<Zone> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CheckUtil.assertNotEmpty(zoneParam.getZoneId(),"删除的zoneId为空"),Zone::getZoneId,zoneParam.getZoneId());
        try {
            return zoneMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(ZoneError.DELETE_ERROR,"存在球桌！");
        }
    }
}
