package com.xiaoyang.billiards.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoyang.billiards.architecture.common.Errors.ConsumeError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.entity.Consume;
import com.xiaoyang.billiards.mapper.ConsumeMapper;
import com.xiaoyang.billiards.params.ConsumeParam;
import com.xiaoyang.billiards.response.ConsumeResponse;
import com.xiaoyang.billiards.response.analyse.DurationResponse;
import com.xiaoyang.billiards.response.analyse.TurnoverResponse;
import com.xiaoyang.billiards.service.ConsumeService;
import com.xiaoyang.billiards.transfer.ConsumeTransfer;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Service
public class ConsumeServiceImpl extends ServiceImpl<ConsumeMapper, Consume> implements ConsumeService {

    @Resource
    private ConsumeMapper consumeMapper;

    @Override
    public Integer add(ConsumeParam consumeParam) {
        Consume consume = new Consume();
        consume.setMemberId(consumeParam.getMemberId());
        consume.setConsumeMoney(consumeParam.getConsumeMoney());
        consume.setStartTime(consumeParam.getStartTime());
        consume.setStopTime(new Timestamp(System.currentTimeMillis()));
        consume.setKeepTime(consumeParam.getKeepTime());
        consume.setTableId(consumeParam.getTableId());
        consume.setZoneId(consumeParam.getZoneId());
        return consumeMapper.insert(consume);
    }

    @Override
    public List<ConsumeResponse> queryForList(ConsumeParam consumeParam) {
        LambdaQueryWrapper<Consume> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Consume::getStartTime);
        List<Consume> consumes = consumeMapper.selectList(wrapper);
        return ConsumeTransfer.INSTANCE.transfer(consumes);
    }

    @Override
    public Integer delete(ConsumeParam consumeParam) {
        LambdaQueryWrapper<Consume> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(consumeParam.getConsumeId() != null,Consume::getConsumeId, consumeParam.getConsumeId());
        try {
            return consumeMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(ConsumeError.DELETE_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer batchDelete(List<Integer> consumeIds) {
        LambdaQueryWrapper<Consume> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(Consume::getConsumeId, consumeIds);
        try {
            return consumeMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(ConsumeError.DELETE_ERROR,e.getMessage());
        }
    }

    @Override
    public List<DurationResponse> duration() {
        return consumeMapper.duration();
    }

    @Override
    public List<TurnoverResponse> turnover() {
        return consumeMapper.turnover();
    }
}
