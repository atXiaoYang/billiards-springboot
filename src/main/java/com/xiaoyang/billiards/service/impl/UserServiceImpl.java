package com.xiaoyang.billiards.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoyang.billiards.architecture.common.Errors.UserError;
import com.xiaoyang.billiards.architecture.common.exception.ServiceException;
import com.xiaoyang.billiards.entity.User;
import com.xiaoyang.billiards.enums.UserEnums;
import com.xiaoyang.billiards.mapper.UserMapper;
import com.xiaoyang.billiards.params.UserParam;
import com.xiaoyang.billiards.response.UserResponse;
import com.xiaoyang.billiards.service.UserService;
import com.xiaoyang.billiards.transfer.UserTransfer;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/2/4
 * @DESCRIPTION TODO
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public UserResponse query(UserParam userParam) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(userParam.getName()) ,User::getUserName, userParam.getName());
        wrapper.eq(StringUtils.hasLength(userParam.getPassword()),User::getUserPassword, userParam.getPassword());
        wrapper.eq(userParam.getUserId() != null,User::getUserId,userParam.getUserId());
        User user = getOne(wrapper);
        UserResponse userResponse = UserTransfer.INSTANCE.transfer(user);
        if (user != null) {
            userResponse.setMsg("登陆成功");
        } else {
            UserResponse err = new UserResponse();
            err.setMsg("用户名或密码错误");
            return err;
        }
        return userResponse;
    }

    @Override
    public List<UserResponse> queryForList(UserParam userParam) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        List<User> users = userMapper.selectList(wrapper);
        return UserTransfer.INSTANCE.transfer(users);
    }

    @Override
    public Integer updateByUserParam(UserParam userParam) {
        LambdaUpdateWrapper<User> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(StringUtils.hasLength(userParam.getName()),User::getUserName,userParam.getName());
        wrapper.set(StringUtils.hasLength(userParam.getPassword()),User::getUserPassword,userParam.getPassword());
        wrapper.set(StringUtils.hasLength(userParam.getUserPhone()),User::getUserPhone,userParam.getUserPhone());
        wrapper.set(StringUtils.hasLength(userParam.getUserCall()),User::getUserCall,userParam.getUserCall());
        wrapper.set(userParam.getUserState() != null,User::getUserState,userParam.getUserState());
        wrapper.eq(userParam.getUserId() != null,User::getUserId,userParam.getUserId());
        try {
            return userMapper.update(wrapper);
        } catch (Exception e) {
            throw new ServiceException(UserError.UPDATE_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer add(UserParam userParam) {
        //insert into user set ?,user_state=1
        User user = new User();
        user.setUserName(userParam.getUserName());
        user.setUserPassword(userParam.getUserPassword());
        user.setUserPhone(userParam.getUserPhone());
        user.setUserCall(userParam.getUserCall());
        user.setUserState(UserEnums.ADMIN.getCode());
        try {
            return userMapper.insert(user);
        } catch (Exception e) {
            throw new ServiceException(UserError.ADD_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer delete(UserParam userParam) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(userParam.getUserId() != null,User::getUserId,userParam.getUserId());
        try {
            return userMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(UserError.DELETE_ERROR,e.getMessage());
        }
    }

    @Override
    public Integer batchDelete(List<Integer> userIds) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(User::getUserId,userIds);
        try {
            return userMapper.delete(wrapper);
        } catch (Exception e) {
            throw new ServiceException(UserError.DELETE_ERROR,e.getMessage());
        }
    }
}
