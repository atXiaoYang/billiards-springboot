package com.xiaoyang.billiards.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoyang.billiards.entity.User;
import com.xiaoyang.billiards.entity.Zone;
import com.xiaoyang.billiards.params.UserParam;
import com.xiaoyang.billiards.params.ZoneParam;
import com.xiaoyang.billiards.response.UserResponse;
import com.xiaoyang.billiards.response.ZoneResponse;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
public interface ZoneService extends IService<Zone> {
    List<ZoneResponse> query(ZoneParam zoneParam);

    Integer updateByZoneParam(ZoneParam zoneParam);

    Integer add(ZoneParam zoneParam);

    Integer delete(ZoneParam zoneParam);
}
