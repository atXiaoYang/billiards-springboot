package com.xiaoyang.billiards.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoyang.billiards.entity.PoolTable;
import com.xiaoyang.billiards.entity.User;
import com.xiaoyang.billiards.params.TableParam;
import com.xiaoyang.billiards.params.UserParam;
import com.xiaoyang.billiards.response.TableResponse;
import com.xiaoyang.billiards.response.UserResponse;
import net.sf.jsqlparser.schema.Table;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
public interface TableService  extends IService<PoolTable> {
    List<TableResponse> query(TableParam tableParam);

    Integer add(TableParam tableParam);

    Integer delete(TableParam tableParam);

    Integer start(TableParam tableParam);

    Integer stop(TableParam tableParam);
}
