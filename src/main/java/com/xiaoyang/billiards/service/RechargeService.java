package com.xiaoyang.billiards.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoyang.billiards.entity.Member;
import com.xiaoyang.billiards.entity.Recharge;
import com.xiaoyang.billiards.params.RechargeParam;
import com.xiaoyang.billiards.response.RechargeResponse;
import com.xiaoyang.billiards.response.analyse.InmoneyResponse;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
public interface RechargeService extends IService<Recharge> {
    Integer add(RechargeParam rechargeParam);

    List<RechargeResponse> queryForList(RechargeParam rechargeParam);

    Integer delete(RechargeParam rechargeParam);

    Integer batchDelete(List<Integer> rechargeIds);

    List<InmoneyResponse> inmoney();
}
