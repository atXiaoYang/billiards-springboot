package com.xiaoyang.billiards.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/5
 * @DESCRIPTION TODO
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RechargeResponse {
    private Integer rechargeId;
    private Integer memberId;
    private BigDecimal rechargeMoney;
    private Timestamp rechargeTime;
}
