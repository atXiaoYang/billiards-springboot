package com.xiaoyang.billiards.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ZoneResponse {
    private Integer zoneId;
    private String zoneName;
    private BigDecimal zoneMoney;
    private BigDecimal memberMoney;
}
