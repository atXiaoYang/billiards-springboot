package com.xiaoyang.billiards.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 杨泽纬
 * @description 测试参数
 * @serviceName TestParam
 * @date 2023/11/16
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

    private Integer userId;
    private String msg;
    private String token;
    private String userName;
    private String userPassword;
    private String userPhone;
    private int userState;
    private String userCall;

    public UserResponse(Integer userId,String msg, String token) {
        this.userId = userId;
        this.msg = msg;
        this.token = token;
    }
}
