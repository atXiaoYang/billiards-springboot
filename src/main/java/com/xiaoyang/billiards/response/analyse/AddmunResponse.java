package com.xiaoyang.billiards.response.analyse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/5
 * @DESCRIPTION TODO
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddmunResponse {
    private String month;
    private Integer number;
}
