package com.xiaoyang.billiards.response.analyse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/5
 * @DESCRIPTION TODO
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InmoneyResponse {
    private String month;
    private BigDecimal sumMoney;
}
