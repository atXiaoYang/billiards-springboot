package com.xiaoyang.billiards.response;

import com.xiaoyang.billiards.entity.PoolTable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sf.jsqlparser.schema.Table;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TableResponse {
    private Integer zoneId;
    private String zoneName;
    private BigDecimal zoneMoney;
    private BigDecimal memberMoney;
    private List<PoolTable> tableInfo;
}
