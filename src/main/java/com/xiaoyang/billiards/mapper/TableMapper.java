package com.xiaoyang.billiards.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoyang.billiards.entity.PoolTable;
import com.xiaoyang.billiards.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/3
 * @DESCRIPTION TODO
 */
@Mapper
public interface TableMapper extends BaseMapper<PoolTable> {
}
