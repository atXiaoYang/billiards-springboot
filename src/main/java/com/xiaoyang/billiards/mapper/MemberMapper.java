package com.xiaoyang.billiards.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoyang.billiards.entity.Consume;
import com.xiaoyang.billiards.entity.Member;
import com.xiaoyang.billiards.response.analyse.AddmunResponse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {
    List<AddmunResponse> addmun();
}
