package com.xiaoyang.billiards.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoyang.billiards.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/2/4
 * @DESCRIPTION TODO
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}

