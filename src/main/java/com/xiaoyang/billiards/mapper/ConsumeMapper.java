package com.xiaoyang.billiards.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoyang.billiards.entity.Consume;
import com.xiaoyang.billiards.entity.PoolTable;
import com.xiaoyang.billiards.response.analyse.DurationResponse;
import com.xiaoyang.billiards.response.analyse.TurnoverResponse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Mapper
public interface ConsumeMapper extends BaseMapper<Consume> {
    List<DurationResponse> duration();

    List<TurnoverResponse> turnover();
}
