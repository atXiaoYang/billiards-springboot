package com.xiaoyang.billiards.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoyang.billiards.entity.Recharge;
import com.xiaoyang.billiards.response.analyse.InmoneyResponse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @AUTHOR XiaoYang
 * @DATE 2024/3/4
 * @DESCRIPTION TODO
 */
@Mapper
public interface RechargeMapper extends BaseMapper<Recharge> {
    List<InmoneyResponse> inmoney();
}
